import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { LayoutService } from './service/app.layout.service';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {

    model: any[] = [];

    constructor(public layoutService: LayoutService) { }

    ngOnInit() {
        this.model = [


            { label: 'Patients', icon: 'pi pi-home', routerLink: ['/patient-list'] },
            { label: 'Waiting', icon: 'pi pi-cog', routerLink: ['/'] },



          
        ];
    }
}
