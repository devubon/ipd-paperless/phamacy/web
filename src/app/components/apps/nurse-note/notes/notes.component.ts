import { Component,Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Location} from '@angular/common';
import { AxiosResponse } from 'axios';
import * as _ from 'lodash';
import { DateTime } from 'luxon';
import { Table } from 'primeng/table';
import { MenuItem, SelectItem } from 'primeng/api';
import { NgxSpinnerService } from 'ngx-spinner';
import { Message, ConfirmationService, MessageService } from 'primeng/api';
import { LookupService } from '../../../../../app/shared/lookup-service';
import { NurseNoteService } from '../nurse-note.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss'],
  providers: [MessageService, ConfirmationService],
})
export class NotesComponent implements OnInit{
  @Input() parentData: any;
  //ken
  anFromParent: any;
  admit_id: any;
  NurseNote: any = [];
  activitydata: any;
  evaluatedata: any;
  problem_list: any;
  
  query: any = '';
  dataSet: any[] = [];
  dataSetReview: any[] = [];
  dataSetAdmit: any[] = [];
  dataSetWaitingInfo: any[] = [];
  dataSetTreatement: any[] = [];
  dataSetPatientAllergy: any[] = [];
  loading = false;
  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;
  wardId: any;
  doctorId: any;
  queryParamsData: any;
  doctorBy: any;
  preDiag: any;
  chieft_complaint: any;
  panelsWard: any[] = [];
  panelsBed: any[] = [];
  bedId: any;
  isLoading: boolean = true;
  isVisible = false;
  userId: any;
  departmentId: any;
  isSaved = false;
  btnSaveIsVisible = true;
  selectedValue: any = null;
  speedDialitems: MenuItem[] = [];
  selectedDrop: SelectItem = { value: '' };
  visibleNursenotesidebar: boolean = false;
  itemActivityInfo: any;
  itemEvaluate: any;
doctors:any = [
  { label: 'Istanbul', value: 1 },
    { label: 'Paris', value: 2 }
];
wards:any = [];
beds:any = [];
dataLoaded: boolean = false;

user:any = [];

items: any[] = [];

activeIndex: number = 0;

// parentData:any;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private nurseNoteService: NurseNoteService,
    private lookupService: LookupService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private spinner: NgxSpinnerService,
    private location: Location,
  ) {
    let jsonString: any = this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
    console.log('queryParamsData : ', this.queryParamsData);
  }

  hideSpinner() {
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }

  async ngOnInit() {
    
    this.parentData = this.queryParamsData;
    this.anFromParent = this.parentData.an;
    console.log( "ngOnInit parentData :",this.parentData);

    this.dataLoaded =true;
    await this.getUserList();
    await this.getNursePatient();
  }

  async getUserList() {
    try {
      const response = await this.nurseNoteService.getUserList();
      let userlist = response.data.data;
      console.log('getUserList :', this.user);
      for(let u of userlist){
        let data : any;
        data = {
          name: u.profile.title + u.profile.fname + ' ' + u.profile.lname,
          user_id: u.user_id
        }
        this.user.push(data);
      }
    } catch (error) {
      console.log(error);
    }
  }

//ken
async getNursePatient(){
  try {
    const response = await this.nurseNoteService.getNursePatient(this.anFromParent);
    this.admit_id = response.data.data[0].admit_id;
    await this.getNursenote(this.admit_id);
  } catch (error) {
    console.log(error);
  }
}
async getNursenote(id: any) {
  try {
    const respone = await this.nurseNoteService.getNurseNote(id);
    const responseData: any = respone.data;
    let data: any = responseData;
    console.log('getNursenote Nurse note :', data);
    this.NurseNote = data.data;
    for(let i of this.NurseNote){
      let date = new Date(i.nurse_note_date);
      i.thai_date = date.toLocaleDateString('th-TH');
      i.user_name = this.user.find((x: any) => x.create_by == i.user_id).name;
    }
  } catch (error: any) {
    console.log(error);
    this.messageService.add({
      severity: 'error',
      summary: 'เกิดข้อผิดพลาด #',
      detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
    });
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }
}
  async navigateWaiting() {
    this.router.navigate(['/waiting-admit']);
  }
  backPage() {
    this.location.back();
  }

  changeItem(i: number) {
      this.activeIndex = i;
  }
}





