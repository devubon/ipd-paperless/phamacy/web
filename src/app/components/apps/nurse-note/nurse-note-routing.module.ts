import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {NurseNoteComponent} from './nurse-note.component'
const routes: Routes = [
  {
    path:'',component:NurseNoteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NurseNoteRoutingModule { }
