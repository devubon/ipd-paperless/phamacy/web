import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { AxiosResponse } from 'axios';
import * as _ from 'lodash';
import { DateTime } from 'luxon';
import { Table } from 'primeng/table';
import { MenuItem, SelectItem } from 'primeng/api';
import { NgxSpinnerService } from 'ngx-spinner';
import { Message, ConfirmationService, MessageService } from 'primeng/api';
import { LookupService } from '../../../shared/lookup-service';
import { NurseNoteService } from './nurse-note.service';

interface AutoCompleteCompleteEvent {
    originalEvent: Event;
    query: string;
}
interface OptionItemsDoctors {
    id: string;
    fname: string;
    default_usage: string;
    item_type_id: string;
}
interface OptionItemsWards {
    id: string;
    name: string;
}
interface OptionItemsBeds {
    bed_id: string;
    name: string;
}

@Component({
    selector: 'app-nurse-note',
    templateUrl: './nurse-note.component.html',
    styleUrls: ['./nurse-note.component.scss'],
    providers: [MessageService, ConfirmationService],
})
export class NurseNoteComponent implements OnInit {
    query: any = '';
    dataSet: any[] = [];
    dataSetReview: any[] = [];
    dataSetAdmit: any[] = [];
    dataSetWaitingInfo: any[] = [];
    dataSetTreatement: any[] = [];
    dataSetPatientAllergy: any[] = [];
    loading = false;

    isActivityChecked = [];
    isEvaluateChecked = [];
    total = 0;
    pageSize = 20;
    pageIndex = 1;
    offset = 0;
    user_login_name: any;

    wardId: any;
    doctorId: any;

    is_select_problem_list: boolean = false;
    is_select_Evaluate: boolean = false;
    is_select_Activity: boolean = false;
    is_requirefield: boolean = false;

    queryParamsData: any;
    patientInfo: any;
    opdInfo: any;
    doctorBy: any;
    preDiag: any;
    chieft_complaint: any;
    panelsWard: any[] = [];
    panelsBed: any[] = [];
    bedId: any;
    isLoading: boolean = true;
    isVisible = false;
    userId: any;
    departmentId: any;
    isSaved = false;
    btnSaveIsVisible = true;
    selectedValue: any = null;
    speedDialitems: MenuItem[] = [];
    selectedDrop: SelectItem = { value: '' };
    visibleNursenotesidebar: boolean = false;
    itemActivityInfo: any;
    itemEvaluate: any;

    cities: any = [

    ];
    doctors: any = [

    ];
    wards: any = [];
    beds: any = [];

    selectedItemsDoctor: any;
    filteredItemsDoctors: any | undefined;
    optionsItemsDoctors: OptionItemsDoctors[] = [];

    selectedItemsWards: any;
    filteredItemsWards: any | undefined;
    optionsItemsWards: OptionItemsWards[] = [];

    selectedItemsBeds: any;
    filteredItemsBeds: any | undefined;
    optionsItemsBeds: OptionItemsBeds[] = [];

    dataLoaded: boolean = false;
    parentData: any;
    txtarea_problem_list: any = '';
    txtarea_activity: any;
    txtarea_evaluate: any;
    activity_checked: any = [];
    evaluate_checked: any = [];
    anFromParent: any;
    admit_id: any;
    optionActivitySelected: any = [];
    optionEvaluateSelected: any = [];
    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private nurseNoteService: NurseNoteService,
        private lookupService: LookupService,
        private messageService: MessageService,
        private confirmationService: ConfirmationService,
        private spinner: NgxSpinnerService,
        private location: Location
    ) {
        let jsonString: any =
            this.activatedRoute.snapshot.queryParamMap.get('data');
        const jsonObject = JSON.parse(jsonString);
        this.queryParamsData = jsonObject;
    }

    hideSpinner() {
        setTimeout(() => {
            this.spinner.hide();
        }, 1000);
    }
    filterItemsDoctors(event: AutoCompleteCompleteEvent) {
        let filtered: any[] = [];
        let query = event.query.toLowerCase();

        for (let i = 0; i < this.optionsItemsDoctors.length; i++) {
            let item = this.optionsItemsDoctors[i];
            if (item.fname.toLowerCase().indexOf(query) !== -1) {
                filtered.push(item);
            }
        }
        this.filteredItemsDoctors = filtered;
    }
    filterItemsWards(event: AutoCompleteCompleteEvent) {
        let filtered: any[] = [];
        let query = event.query.toLowerCase();

        for (let i = 0; i < this.optionsItemsWards.length; i++) {
            let item = this.optionsItemsWards[i];
            if (item.name.toLowerCase().indexOf(query) !== -1) {
                filtered.push(item);
            }
        }
        this.filteredItemsWards = filtered;
    }
    filterItemsBeds(event: AutoCompleteCompleteEvent) {
        let filtered: any[] = [];
        let query = event.query.toLowerCase();

        for (let i = 0; i < this.optionsItemsBeds.length; i++) {
            let item = this.optionsItemsBeds[i];
            if (item.name.toLowerCase().indexOf(query) !== -1) {
                filtered.push(item);
            }
        }
        this.filteredItemsBeds = filtered;
    }
    onSelectedWard(e: any) {
        let data = e;
        this.getBed(data.id);
    }
    onSelectedBed(e: any) {

        let data = e;
    }
    buttonSpeedDial() {
        this.speedDialitems = [
            {
                icon: 'pi pi-arrow-left',
                routerLink: ['/patient-list'],
                tooltipOptions: {
                    tooltipLabel: 'ย้อนกลับ',
                    tooltipPosition: 'left',
                },
            },
            {
                icon: 'fa-solid fa-circle-info',
                command: () => {
                    // this.navigatePatientInfo()
                },
                tooltipOptions: {
                    tooltipLabel: 'Patient Info',
                    tooltipPosition: 'left',
                },
            },
            {
                icon: 'fa-solid fa-user-doctor',
                command: () => {
                    // this.navigateDoctorOrder()
                },
                tooltipOptions: {
                    tooltipLabel: 'Doctor Order',
                    tooltipPosition: 'left',
                },
            },
            {
                icon: 'fa-solid fa-heart-pulse',
                command: () => {
                    // this.navigateEkg()
                },
                tooltipOptions: {
                    tooltipLabel: 'EKG',
                    tooltipPosition: 'left',
                },
            },
            {
                icon: 'fa-solid fa-phone-volume',
                command: () => {
                    // this.navigateConsult()
                },
                tooltipOptions: {
                    tooltipLabel: 'Consult',
                    tooltipPosition: 'left',
                },
            },
            {
                icon: 'fa-solid fa-vial-virus',
                command: () => {
                    // this.navigateLab()
                },
                tooltipOptions: {
                    tooltipLabel: 'Lab',
                    tooltipPosition: 'left',
                },
            },
        ];
    }

    async ngOnInit() {
        this.parentData = await this.queryParamsData;
        console.log('query param:',this.parentData);
        this.anFromParent = await this.parentData.an;

        this.dataLoaded = true;
        await this.getNursePatient();
        await this.listActivity();
        await this.listEvaluate();

        this.buttonSpeedDial();
        this.getWaitingInfo();

        await this.getReview();
        await this.getAdmit();
        await this.getWard();
        await this.getDoctor();
        await this.getTreatement();
        await this.getPatientAllergy();
        await this.assignCollapseWard();
    }
    //ken
    async getNursePatient() {
      console.log('anFromParent:',this.anFromParent);
        try {
            const response = await this.nurseNoteService.getNursePatient(
                this.anFromParent
            );

            this.admit_id = response.data.data[0].admit_id;
        } catch (error) {
            console.log(error);
        }
    }

    async SaveNursenote(): Promise<void> {
        await this.saveNurseNotedata();
    }
    async saveNurseNotedata() {
        var date_now = DateTime.now();
        const datenow = date_now.toFormat('yyyy-MM-dd');
        const timenow = date_now.toFormat('HH:mm:ss');
        this.optionActivitySelected.push(this.txtarea_activity);
        this.optionEvaluateSelected.push(this.txtarea_evaluate);

        let data: any = {
            nurse_note_date: datenow,
            nurse_note_time: timenow,
            problem_list: [this.txtarea_problem_list],
            activity: this.optionActivitySelected,
            evaluate: this.optionEvaluateSelected,
            admit_id: this.admit_id,
            create_date: datenow,
            create_by: sessionStorage.getItem('user_id'),
            modify_date: datenow,
            modify_by: sessionStorage.getItem('user_id'),
            is_active: true,
            item_used: [{}],
        };

        try {
            const respone = await this.nurseNoteService.saveNurseNote(data);
            if (respone.status === 200) {
                // alert("บันทึกสำเร็จ");
                this.visibleNursenotesidebar = false;
                this.messageService.add({
                    severity: 'success',
                    summary: 'บันทึกสำเร็จ #',
                    detail: 'บันทึกทางการพยาบาลสำเร็จกรุณาตรวจสอบข้อมูลหลังบันทึก...',
                });
                window.location.reload();
            } else {

                this.messageService.add({
                    severity: 'error',
                    summary: 'เกิดข้อผิดพลาด #',
                    detail: 'กรุณาติดต่อ admin ...',
                });
            }
        } catch (error: any) {}
    }
    disable_botton_save() {
        if (
            this.is_select_Activity == true &&
            this.is_select_Evaluate == true &&
            this.is_select_problem_list == true
        ) {
            this.is_requirefield = true;
        } else {
            this.is_requirefield = false;
        }
    }
    optionActivityClick(e: any) {
        this.optionActivitySelected = e.checked;
        if (this.optionActivitySelected.length > 0) {
            this.is_select_Activity = true;
        } else {
            this.is_select_Activity = false;
        }
        this.disable_botton_save();
    }

    keypress_problem_list(e: any) {
        if (this.txtarea_problem_list) {
            this.is_select_problem_list = true;
        } else {
            this.is_select_problem_list = false;
        }
        this.disable_botton_save();
    }

    optionEvaluateClick(e: any) {
        this.optionEvaluateSelected = e.checked;
        if (this.optionEvaluateSelected.length > 0) {
            this.is_select_Evaluate = true;
        } else {
            this.is_select_Evaluate = false;
        }
        this.disable_botton_save();
    }
    async openSlidebarnursenote() {
        this.visibleNursenotesidebar = true;

        if (this.is_requirefield == true) {
            this.is_requirefield = false;
            this.isEvaluateChecked = [];
            this.isActivityChecked = [];
            this.txtarea_problem_list = '';
            this.txtarea_evaluate = '';
            this.txtarea_activity = '';
        }
    }
    async listActivity() {
        try {
            const respone = await this.nurseNoteService.getActivity();
            this.itemActivityInfo = respone.data.data;
        } catch (error: any) {}
    }
    async listEvaluate() {
        try {
            const respone = await this.nurseNoteService.getEvaluate();
            this.itemEvaluate = respone.data.data;
        } catch (error: any) {}
    }

    async getReview() {
        this.spinner.show();
        try {
            const response = await this.nurseNoteService.getReview(
                this.queryParamsData.an
            );
            const data = await response.data;
            this.dataSetReview = await data;
            this.opdInfo = this.dataSetReview[0];

            this.isLoading = false;
            this.hideSpinner();

        } catch (error: any) {

            this.hideSpinner();

            this.messageService.add({
                severity: 'error',
                summary: 'เกิดข้อผิดพลาด #',
                detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
            });
        }
    }

    async getTreatement() {
        try {
            const response = await this.nurseNoteService.getTreatement(
                this.queryParamsData.an
            );
            const data = response.data;
            this.dataSetTreatement = await data;
 
        } catch (error: any) {

            this.hideSpinner();

            this.messageService.add({
                severity: 'error',
                summary: 'เกิดข้อผิดพลาด #ข้อมูลการรักษา',
                detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
            });
        }
    }

    async getPatientAllergy() {
        try {
            const response = await this.nurseNoteService.getPatientAllergy(
                this.queryParamsData.an
            );
            const data = response.data;
            this.dataSetPatientAllergy = await data;

        } catch (error: any) {

            this.hideSpinner();

            this.messageService.add({
                severity: 'error',
                summary: 'เกิดข้อผิดพลาด #ข้อมูลแพ้ยา',
                detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
            });
        }
    }

    async getAdmit() {

        try {
            const response = await this.nurseNoteService.getWaitingAdmit(
                this.queryParamsData.an
            );
            const data = await response.data;

            this.dataSetAdmit = await data;
            if (!this.dataSetAdmit[0]) {

                this.btnSaveIsVisible = false;
            }

            setTimeout(() => {
                this.spinner.hide();
            }, 1000);
        } catch (error: any) {

            this.hideSpinner();

            this.messageService.add({
                severity: 'error',
                summary: 'เกิดข้อผิดพลาด #ข้อมูล admit',
                detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
            });
            setTimeout(() => {
                this.spinner.hide();
            }, 1000);
        }
    }
    async getWaitingInfo() {
      console.log('an:',this.queryParamsData.an);
        try {
            const response = await this.nurseNoteService.getWaitingInfo(
                this.queryParamsData.an
            );

            const data = await response.data;
            this.dataSetWaitingInfo = await data;
            this.patientInfo = await this.dataSetWaitingInfo[0];
            this.preDiag = await this.dataSetWaitingInfo[0].pre_diag;
            this.doctorBy = await this.dataSetWaitingInfo[0].admit_by;


            if (!this.doctorBy || !this.preDiag) {
                this.messageService.add({
                    severity: 'error',
                    summary: 'เกิดข้อผิดพลาด #ข้อมูลการวินิจฉัยเบื้องต้น',
                    detail: 'กรุณาติดต่อผู้ดูแลระบบ...',
                });
                this.btnSaveIsVisible = false;
            }

            setTimeout(() => {
                this.spinner.hide();
            }, 1000);
        } catch (error: any) {

            this.messageService.add({
                severity: 'error',
                summary: 'เกิดข้อผิดพลาด # ข้อมูลผู้ป่วย',
                detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
            });
            setTimeout(() => {
                this.spinner.hide();
            }, 1000);
        }
    }
    async getWard() {
        try {
            const response: AxiosResponse = await this.lookupService.getWard();
            const responseData: any = response.data;
            const data: any = responseData.data;
            this.wards = data;

            this.optionsItemsWards = responseData.data;

        } catch (error: any) {

            this.hideSpinner();

            this.messageService.add({
                severity: 'error',
                summary: 'เกิดข้อผิดพลาด #ข้อมูล ward',
                detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
            });
        }
    }

    async getDoctor() {
        try {
            const response: AxiosResponse =
                await this.lookupService.getDoctor();
            const responseData: any = response.data;
            const data: any = responseData.data;

            this.optionsItemsDoctors = responseData.data;

            this.doctors = data;
            if (!_.isEmpty(data)) {
                
            }
        } catch (error: any) {

            this.hideSpinner();

            this.messageService.add({
                severity: 'error',
                summary: 'เกิดข้อผิดพลาด #ข้อมูลแพทย์',
                detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
            });
        }
    }

    assignCollapseWard() {
        this.panelsWard = [
            {
                active: true,
                name: 'เลือกตึก',
                disabled: false,
            },
        ];
    }

    assignCollapseBed() {
        this.panelsBed = [
            {
                active: true,
                name: 'เลือกเตียง',
                disabled: false,
            },
        ];
    }

    setWard(data: any) {
        this.wardId = data.id;
        let wardName = data.name;
        this.departmentId = data.department_id;
        this.panelsWard = [
            {
                active: false,
                name: 'เลือกตึก : ' + wardName,
                disabled: false,
            },
        ];

        this.bedId = '';
        this.getBed(this.wardId);
    }

    async getBed(wardId: any) {
        try {

            const response: AxiosResponse = await this.lookupService.getBed(
                wardId
            );
            const responseData: any = response.data;
            const data: any = responseData.data;
            this.beds = data;

            this.optionsItemsBeds = responseData.data;
        } catch (error: any) {
            this.hideSpinner();

            this.messageService.add({
                severity: 'error',
                summary: 'เกิดข้อผิดพลาด # ข้อมูลเตียง',
                detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
            });
        }
    }

    setBed(data: any) {

        this.bedId = data.bed_id;
        let bedName = data.name;
        this.panelsBed = [
            {
                active: false,
                name: 'เลือกเตียง : ' + bedName,
                disabled: false,
            },
        ];
    }

    logOut() {
        sessionStorage.setItem('token', '');
        return this.router.navigate(['/login']);
    }

    async saveRegister() {
        if (!this.selectedItemsWards.id && !this.selectedItemsBeds.bed_id) {
            this.messageService.add({
                severity: 'error',
                summary: 'เกิดข้อผิดพลาด !',
                detail: 'กรูณาตรวจสอบกรอกข้อมูลให้ครบ',
            });
        } else {
            this.isSaved = true;
            this.spinner.show();
            let info: any = {
                patient: this.dataSetWaitingInfo,
                review: this.dataSetReview,
                treatment: this.dataSetTreatement,
                admit: this.dataSetAdmit,
                bed: [
                    {
                        bed_id: this.selectedItemsBeds.bed_id,
                        status: 'Used',
                    },
                ],
                ward: [
                    {
                        ward_id: this.selectedItemsWards.id,
                    },
                ],
                doctor: [{ user_id: this.selectedItemsDoctor.id }],
                department: [
                    { department_id: this.selectedItemsWards.department_id },
                ],
                patient_allergy: this.dataSetPatientAllergy,
            };
       
            try {
                const response = await this.nurseNoteService.saveRegister(info);
            
                if (response.status === 200) {
                    this.hideSpinner();
                    this.messageService.add({
                        severity: 'success',
                        summary: 'บันทึกสำเร็จ #',
                        detail: 'รอสักครู่...',
                    });
                    this.isSaved = true;
                    setTimeout(() => {
                        this.navigateWaiting();
                    }, 2000);
                }
            } catch (error) {
                
                this.hideSpinner();
                this.messageService.add({
                    severity: 'error',
                    summary: 'เกิดข้อผิดพลาด !',
                    detail: 'กรูณาตรวจสอบ',
                });
            }
        }
    }

    async navigateWaiting() {

        this.router.navigate(['/waiting-admit']);

    }
    backPage() {
        this.location.back();
    }
}
