import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Location} from '@angular/common';
import { AxiosResponse } from 'axios';
import * as _ from 'lodash';
import { DateTime } from 'luxon';
import { Table } from 'primeng/table';
import { MenuItem, SelectItem } from 'primeng/api';
import { NgxSpinnerService } from 'ngx-spinner';
import { Message, ConfirmationService, MessageService } from 'primeng/api';
import { LookupService } from '../../../shared/lookup-service';

import { RegisterService } from './register.service';

interface AutoCompleteCompleteEvent {
  originalEvent: Event;
  query: string;
}
interface OptionItemsDoctors {
  id: string;
  fname: string;
  default_usage: string;
  item_type_id: string;
}
interface OptionItemsWards {
  id: string;
  name: string;  
}
interface OptionItemsBeds {
  bed_id: string;
  name: string;
  type_name:string;
  namebed:string;  
}


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [MessageService, ConfirmationService],

})
export class RegisterComponent implements OnInit {
  query: any = '';
  dataSet: any[] = [];
  dataSetReview: any[] = [];
  dataSetAdmit: any[] = [];
  dataSetWaitingInfo: any[] = [];
  dataSetTreatement: any[] = [];
  dataSetPatientAllergy: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;

  wardId: any;
  doctorId: any;
  is_select_doctor: boolean=false;    
  is_select_ward: boolean=false;  
  is_select_bed: boolean=false;  
  is_select_register: boolean=false;  
  queryParamsData: any;
  doctorBy: any;
  preDiag: any;
  chieft_complaint: any;
  panelsWard: any[] = [];
  panelsBed: any[] = [];
  bedId: any;
  isLoading: boolean = true;
  isVisible = false;
  userId: any;
  departmentId: any;
  isSaved = false;
  btnSaveIsVisible = true;
  selectedValue: any = null;

  speedDialitems: MenuItem[] = [];


  selectedDrop: SelectItem = { value: '' };


 
  cities:any = [
    { label: 'New York', value: { id: 1, name: 'New York', code: 'NY' } },
    { label: 'Rome', value: { id: 2, name: 'Rome', code: 'RM' } },
    { label: 'London', value: { id: 3, name: 'London', code: 'LDN' } },
    { label: 'Istanbul', value: { id: 4, name: 'Istanbul', code: 'IST' } },
    { label: 'Paris', value: { id: 5, name: 'Paris', code: 'PRS' } }
];
doctors:any = [
  { label: 'Istanbul', value: 1 },
    { label: 'Paris', value: 2 }
];
wards:any = [];
beds:any = [];

selectedItemsDoctor: any;
filteredItemsDoctors: any | undefined;
optionsItemsDoctors: OptionItemsDoctors[] = [];

selectedItemsWards: any;
filteredItemsWards: any | undefined;
optionsItemsWards: OptionItemsWards[] = [];

selectedItemsBeds: any;
filteredItemsBeds: any | undefined;
optionsItemsBeds: OptionItemsBeds[] = [];
itemsBed: any[] | undefined;



  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,

    private registerService: RegisterService,

    private lookupService: LookupService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private spinner: NgxSpinnerService,
    private location: Location
  ) {
    let jsonString: any = this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
    // console.log('queryParamsData : ', this.queryParamsData);

  }

  hideSpinner() {
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }
  filterItemsDoctors(event: AutoCompleteCompleteEvent) {
    let filtered: any[] = [];
    let query = event.query.toLowerCase();

    for (let i = 0; i < this.optionsItemsDoctors.length; i++) {
        let item = this.optionsItemsDoctors[i];
        // console.log(item);
        if (item.fname.toLowerCase().indexOf(query) !== -1) {
            filtered.push(item);
        }
    }
    this.filteredItemsDoctors = filtered;
}
filterItemsWards(event: AutoCompleteCompleteEvent) {
  let filtered: any[] = [];
  let query = event.query.toLowerCase();

  for (let i = 0; i < this.optionsItemsWards.length; i++) {
      let item = this.optionsItemsWards[i];
      // console.log(item);
      if (item.name.toLowerCase().indexOf(query) !== -1) {
          filtered.push(item);
      }
  }
  this.filteredItemsWards = filtered;
}
filterItemsBeds(event: AutoCompleteCompleteEvent) {
  let filtered: any[] = [];
  let query = event.query.toLowerCase();

  for (let i = 0; i < this.optionsItemsBeds.length; i++) {
      let item = this.optionsItemsBeds[i];
      // console.log('item',item);
      if (item.name.toLowerCase().indexOf(query) !== -1) {
         item.namebed = item.name + ':' + item.type_name
         filtered.push(item);
      }
  }
  this.filteredItemsBeds = filtered;
}
onSelectedWard(e: any) {
  // console.log(e);
  let data = e;
  this.getBed(data.id);
  this.is_select_ward=true;
  if( this.is_select_doctor == true &&  this.is_select_ward  == true   &&   this.is_select_bed  == true ){
    this.is_select_register=true;

  }
}
onSelecteDoctor(e: any) {
  // console.log(e);
  let data = e;
  //this.getBed(data.id);
  this.is_select_doctor=true;
  if( this.is_select_doctor == true &&  this.is_select_ward  == true   &&   this.is_select_bed  == true ){
    this.is_select_register=true;

  }
}
onSelectedBed(e: any) {
  // console.log(e);
  let data = e;
  // console.log(this.selectedItemsBeds);
  this.is_select_bed=true;

  if( this.is_select_doctor == true &&  this.is_select_ward  == true   &&   this.is_select_bed  == true ){
    this.is_select_register=true;

  }
}
  buttonSpeedDial(){
    this.speedDialitems = [
      {
        icon: 'pi pi-arrow-left',
        routerLink: ['/list-patient'],
        tooltipOptions: {
          tooltipLabel: 'ย้อนกลับ',
          tooltipPosition: 'left'
        },
      },
	  {
        icon: 'fa-solid fa-circle-info',
        command: () => {
          // this.navigatePatientInfo()
        },
        tooltipOptions: {
          tooltipLabel: 'Patient Info',
          tooltipPosition: 'left'
        },
      },
      {
        icon: 'fa-solid fa-user-doctor',
        command: () => {
          // this.navigateDoctorOrder()
        },
        tooltipOptions: {
          tooltipLabel: 'Doctor Order',
          tooltipPosition: 'left'
        },
      },
      {
        icon: 'fa-solid fa-heart-pulse',
        command: () => {
          // this.navigateEkg()
        },
        tooltipOptions: {
          tooltipLabel: 'EKG',
          tooltipPosition: 'left'
        },
      },
      {
        icon: 'fa-solid fa-phone-volume',
        command: () => {
          // this.navigateConsult()
        },
        tooltipOptions: {
          tooltipLabel: 'Consult',
          tooltipPosition: 'left'
        },
      },
      {
        icon: 'fa-solid fa-vial-virus',
        command: () => {
          // this.navigateLab()
        },
        tooltipOptions: {
          tooltipLabel: 'Lab',
          tooltipPosition: 'left'
        },
      }
    ];
  }


  async ngOnInit() {
    await this.buttonSpeedDial();
    await this.getWaitingInfo();
    await this.getReview();
    await this.getAdmit();
    await this.getWard();
    await this.getDoctor();
    await this.getTreatement();
    await this.getPatientAllergy();
    // this.assignCollapseWard();

  }


  async getReview() {
    this.spinner.show();
    try {
      const response = await this.registerService.getReview(this.queryParamsData.an)
      const data = await response.data;
      this.dataSetReview = await data;
      // console.log('getReview : ', this.dataSetReview);
      this.isLoading = false;
      this.hideSpinner();
      // this.notificationService.notificationSuccess('คำชี้แจ้ง', 'กระบวนการทำงานสำเร็จ..', 'top');
    } catch (error: any) {
      // console.log(error);
      this.hideSpinner();

      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });

    }
  }

  async getTreatement() {
    try {
      const response = await this.registerService.getTreatement(this.queryParamsData.an)
      const data = response.data;
      this.dataSetTreatement = await data;
      // console.log('getTreatement : ', this.dataSetTreatement);
    } catch (error: any) {
      // console.log(error);
      this.hideSpinner();

      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });
    }
  }

  async getPatientAllergy() {
    try {
      const response = await this.registerService.getPatientAllergy(this.queryParamsData.an)
      const data = response.data;
      this.dataSetPatientAllergy = await data;
      // console.log('getPatientAllergy : ', this.dataSetPatientAllergy);
    } catch (error: any) {
      // console.log(error);
      this.hideSpinner();

      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });
    }
  }

  async getAdmit() {
    // console.log('getAdmit');
    // console.log(this.queryParamsData.an);
    try {
      const response = await this.registerService.getWaitingAdmit(this.queryParamsData.an);
      const data = await response.data;
      // console.log(data);
      this.dataSetAdmit = await data;
      if (!this.dataSetAdmit[0]) {
        // this.notificationService.notificationError('คำชี้แจ้ง', 'ไม่พบข้อมูล Admit!', 'top');
        this.btnSaveIsVisible = false;
      }
      // console.log('getAdmit : ', this.dataSetAdmit);
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
    } catch (error: any) {
      // console.log(error);

      this.hideSpinner();

      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);

    }
  }
  async getWaitingInfo() {
    // console.log('getWaitingInfo');
    try {
      const response = await this.registerService.getWaitingInfo(this.queryParamsData.an)
      console.log("getWaitingInfo : ",this.queryParamsData.an);
      const data = await response.data;
      this.dataSetWaitingInfo = await data;
      this.preDiag = this.dataSetWaitingInfo[0].pre_diag;
      this.doctorBy = this.dataSetWaitingInfo[0].admit_by
      if (!this.doctorBy || !this.preDiag) {
        this.messageService.add({
          severity: 'error',
          summary: 'เกิดข้อผิดพลาด (getWaitingInfo)',
          detail: '   ไม่พบแพทย์ หรือ ไม่พบDx...',
        });
        this.btnSaveIsVisible = false;
      }
      console.log('getWaitingInfo : ', this.dataSetWaitingInfo);
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
    } catch (error: any) {
      // console.log(error);
      this.hideSpinner();

      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด (getWaitingInfo)',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);

    }
  }
  async getWard() {
    try {
      const response: AxiosResponse = await this.lookupService.getWard();
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.wards = data;
      // console.log('Ward : ', this.wards);
      this.optionsItemsWards = responseData.data;
      // console.log(this.optionsItemsWards);
    } catch (error: any) {
      // console.log(error);
      this.hideSpinner();

      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });

    }
  }

  async getDoctor() {
    try {
      const response: AxiosResponse = await this.lookupService.getDoctor();
      const responseData: any = response.data;
      const data: any = responseData.data;
      for(let i of data){
        i.fname = i.fname +' '+ i.lname;
    }
      this.doctors = data;
      console.log('doctors : ', this.doctors);
      this.optionsItemsDoctors = responseData.data;
      if (!_.isEmpty(data)) {
      }
    } catch (error: any) {
      this.hideSpinner();
      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });

    }
  }

  assignCollapseWard() {
    this.panelsWard = [
      {
        active: true,
        name: 'เลือกตึก',
        disabled: false
      },
    ];
  }

  assignCollapseBed() {
    this.panelsBed = [
      {
        active: true,
        name: 'เลือกเตียง',
        type_name:'',
        disabled: false
      },
    ];
  }

  setWard(data: any) {
    // console.log(data);

    this.wardId = data.id;
    let wardName = data.name;
    this.departmentId = data.department_id;
    this.panelsWard = [
      {
        active: false,
        name: 'เลือกตึก : ' + wardName,
        disabled: false
      },
    ];
    // console.log('wardId_now : ', this.wardId);
    this.bedId = '';
    this.getBed(this.wardId);
  }

  async getBed(wardId: any) {
    try {
      // console.log('wardsId : ', wardId);
      const response: AxiosResponse = await this.lookupService.getBed(wardId);
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.beds = data;
     
      // console.log('Beds : ', this.beds);
      this.optionsItemsBeds = responseData.data;
    } catch (error: any) {
      // console.log(error);
      this.hideSpinner();

      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });


    }
  }

  setBed(data: any) {
    // console.log('dataBeds : ', data);

    this.bedId = data.bed_id;
    let bedName = data.name;
    this.panelsBed = [
      {
        active: false,
        name: 'เลือกเตียง : ' + bedName,type_name:'',
        disabled: false
      },
    ];
    // console.log('bedId : ', this.bedId);
  }

  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }

 
  async saveRegister() {
    // console.log('this.filteredItemsDoctors',this.filteredItemsDoctors);
    // console.log('this.filterItemsWards',this.filterItemsWards);
    // console.log('this.filterItemsBeds',this.filterItemsBeds);
    // console.log('this.selectedItemsDoctor : ', this.selectedItemsDoctor);
    // console.log('this.selectedItemsWards : ', this.selectedItemsWards);
    // console.log('this.selectedItemsBeds : ', this.selectedItemsBeds);
    let valuedetail = "";
    if (this.selectedItemsDoctor == undefined) {
      valuedetail += "โปรดเลือกแพทย์เจ้าของไข้\r\n";
    }
    if (this.selectedItemsWards == undefined) {
      valuedetail += "โปรดเลือกตึกผู้ป่วย\r\n";
    }
    if (this.selectedItemsBeds == undefined) {
      valuedetail += "โปรดเลือกเตียง\r\n";
    }
    console.log("valuedetail : " ,valuedetail);
    if(valuedetail !== ""){
      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด !',
        detail: valuedetail,
      });
    }else{
      console.log("saveRegister : ",this.wardId);
      this.isSaved = true;
      this.spinner.show();
      let info: any = {
        "patient": this.dataSetWaitingInfo,
        "review": this.dataSetReview,
        "treatment": this.dataSetTreatement,
        "admit": this.dataSetAdmit,
        "bed": [
          {
            "bed_id": this.selectedItemsBeds.bed_id,
            "status": "Used"
          }
        ],
        "ward": [
          {
            "ward_id": this.selectedItemsWards.id,
          }
        ],
        "doctor": [
          { "user_id": this.selectedItemsDoctor.id }
        ],
        "department": [
          { "department_id": this.selectedItemsWards.department_id }
        ],
        "patient_allergy": this.dataSetPatientAllergy
      }
      console.log("saveRegister : ",info);
      try {
        const response = await this.registerService.saveRegister(info);
        // console.log(response);
        if (response.status === 200) {
          this.hideSpinner();
          this.messageService.add({
            severity: 'success',
            summary: 'บันทึกสำเร็จ #',
            detail: 'รอสักครู่...',
          });
          this.isSaved = true;
          setTimeout(() => {
            this.navigateWaiting();
          }, 2000);
  
        }
  
      } catch (error) {
        console.log("saveRegister : ",error);
        this.hideSpinner();
        this.messageService.add({
          severity: 'error',
          summary: 'เกิดข้อผิดพลาด !',
          detail: 'กรูณาตรวจสอบ',
        });
      }
    }
  }

  async navigateWaiting() {
    // const confirm = await this.showConfirmModal('คำชี้แจ้ง', 'คุณยังไม่ได้บันทึกข้อมูล คุณแน่ใจว่าจะยกเลิกกระบวนการนี้ !');

    // if (confirm) {
    //   this.isSaved = true;
    this.router.navigate(['/waiting-admit']);
    //   }
  }
  backPage() {
    this.location.back();
  }

async getBedid(){
  this.itemsBed = [];
  for (let i = 0; i < 10000; i++) {
      this.itemsBed.push({ label: 'name'});
  } 
  // console.log('x',this.itemsBed);
  
}

}
