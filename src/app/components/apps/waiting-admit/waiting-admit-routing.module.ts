import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {WaitingAdmitComponent} from './waiting-admit.component';

const routes: Routes = [
  {
    path:'',component:WaitingAdmitComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WaitingAdmitRoutingModule { }
