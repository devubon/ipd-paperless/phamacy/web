import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeWardComponent } from './change-ward.component';

describe('ChangeWardComponent', () => {
  let component: ChangeWardComponent;
  let fixture: ComponentFixture<ChangeWardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ChangeWardComponent]
    });
    fixture = TestBed.createComponent(ChangeWardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
