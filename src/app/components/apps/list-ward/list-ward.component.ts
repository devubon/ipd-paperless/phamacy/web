import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AxiosResponse } from 'axios';

import { NgxSpinnerService } from 'ngx-spinner';
import { ListWardService } from './list-ward.service';
import { SelectItem } from 'primeng/api';
import { DataView } from 'primeng/dataview';


@Component({
    selector: 'app-list-ward',
    templateUrl: './list-ward.component.html',
    styleUrls: ['./list-ward.component.scss'],
})
export class ListWardComponent implements OnInit {
    query: any = '';
    dataSet: any[] = [];
    loading = false;
    total = 0;
    pageSize = 20;
    pageIndex = 1;
    offset = 0;
    user_login_name: any;
    item: any = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    wards: any = [];

    sortOptions: SelectItem[] = [];

    sortOrder: number = 0;

    sortField: string = '';

    sourceCities: any[] = [];

    targetCities: any[] = [];

    orderCities: any[] = [];
    filterName:string = '';

    constructor(
        private router: Router,

        private listWardService: ListWardService,
        private spinner: NgxSpinnerService
    ) {
        this.user_login_name = sessionStorage.getItem('userLoginName');
    }

    ngOnInit(): void {
        this.sortOptions = [
            { label: 'ก --> ฮ', value: 'name' },
            { label: 'ฮ --> ก', value: '!name' },
        ];
        this.getWard();
    }
    /////////////เมธอด หลัก///////////ที่ต้องมี///////////////
    hideSpinner() {
        setTimeout(() => {
            this.spinner.hide();
        }, 1000);
    }

    async getWard() {
        this.spinner.show();
        try {
            const response: AxiosResponse =
                await this.listWardService.getWard();
            const responseData: any = response.data;
            const data: any = responseData.data;
            this.wards = data; // .filter((item: any) => item.is_active == true);
            console.log('Ward : ', data);

            this.hideSpinner();
        } catch (error: any) {
            console.log(error);
            this.hideSpinner();
            // this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');
        }
    }

    onSortChange(event: any) {
        const value = event.value;

        if (value.indexOf('!') === 0) {
            this.sortOrder = -1;
            this.sortField = value.substring(1, value.length);
        } else {
            this.sortOrder = 1;
            this.sortField = value;
        }
    }

    onFilter(dv: DataView, event: Event) {
        console.log(event);
        console.log((event.target as HTMLInputElement).value);
        dv.filter((event.target as HTMLInputElement).value);
    }
    clearFilter(dv: DataView, event: Event) {
        (event.target as HTMLInputElement).value = '';
        dv.filter((event.target as HTMLInputElement).value);
        this.filterName = '';
        
       
    }

    navigatePatient(data: any) {
        console.log(data);
        let jsonString = JSON.stringify(data.id);
        let jsonStringWardName = JSON.stringify(data.name);
        console.log(jsonString);
        // this.closeSidebar();
        localStorage.setItem('ward_id', data.id);
        // this.router.navigate(['/doctor/list-patient'], { queryParams: { data: jsonString } });
        this.router.navigate(['/patient-list'], {
            queryParams: { data: jsonString ,wardName:jsonStringWardName},
        });
    }
}
